# Homework Information 

| 💻          | **Date**       | **Project Folder** | memo |
| :---:       |  :------:   |:---------      | :--------------------   |
| Homework 1  | 2019-12-03  | - [Python-Practice-Numpy](/Python-Practice/20191203/NumPy%20Exercises-JW.ipynb)      | |
| Homework 2  | 2019-12-17  | - [Python-Practice-Pandas](/Python-Practice/20191217/Exercise/Pandas-Exercise-JW.ipynb)      | |
| Homework 3  | 2019-12-31  | - [Python-Practice-NLTK](/Python-Practice/20191231/Execise/Exercise-JW.ipynb)      | |
